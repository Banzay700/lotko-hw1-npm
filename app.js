import browserSync from 'browser-sync'

export const runServer = () => {
  browserSync.init({
    server: {
      baseDir: './',
    },
    startPath: '/index.html',
    notify: false,
    port: 3000,
  })
}

console.log('Hello Node')

runServer()
